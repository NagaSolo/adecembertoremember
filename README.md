### Overview
- repo: gitlab.com/NagaSolo/202011_tsaagun

### Summary

### Backend

Various challenges for tsaagun `EDABIT`

- 01st December - `Edabit` - WhoAmI - `Expert`
    - 5mins - understanding questions
    - 25mins - write, testing, coding craft, implementing **kwargs, dict.items()
    - class[constructor, **kwargs) dict(.items(), .get())

- [UNSOLVED] - 01st December - `Edabit` - BadModIndicator - `gExpert`
    - 60mins - understanding questions
    - 60mins - write, testing, coding craft

- 01st December - `Edabit` - DMVQueue - `VeryHard`
    - 15mins - understanding questions
    - 30mins - write, testing, coding craft, implementing sublist from list using comprehension
    - matrix, comprehension(list), slicing(list, .index())

- 01st December - `Edabit` - ClassEmpWArgsKwargs - `VeryHard`
    - 5mins - understanding questions
    - 10mins - write, testing, coding craft, implementing **kwargs assignment
    - class[constructor, *args, **kwargs), dict.items(), slicing(sting, tuple)

- 02nd December - `Edabit` - FreshMadePizzas - `VeryHard`
    - 5mins - understanding questions, tricky tests
    - 25mins - write, testing, coding craft, implementing @staticmethod, assigning id to object
    - class[constructor, @staticmethod, @classmethod]

- 03rd December - `Edabit` - Geo3TriPerimeter - `Hard`
    - 10mins - understanding questions, tricky tests, understanding coordinates of triangle on matrix
    - 10mins - write, testing, coding craft, getting length from two point
    - math(geometry, sqrt), matrix(coordinate), abs(), {:.2f}, float()

- 03rd December - `Edabit` - ShapeInShape - `Hard`
    - 10mins - understanding questions, tricky challenge
    - 25mins - write, testing, coding craft, implementing string for eval
    - math(geometry, sqrt, pi), dict[get()], eval(), bool logic 

- 04th December - `Edabit` - PLChamp - `VeryHard`
    - 10mins - understanding questions
    - 15mins - write, testing, coding craft, implementation of key and value
    - dict[key, value], sorted(key, reverse)

- 04th December - `Edabit` - CheckRubiks - `VeryHard`
    - 20mins - understanding questions, strategizing implementation
    - 25mins - write, testing, coding craft, bulk of time spent on fixing logic of comparator
    - comprehension(list), list[len()], max(), one-liner[conditionals]

- 04th December - `Edabit` - FruitSmoothie - `VeryHard`
    - 10mins - understanding questions, tricky implementation
    - 25mins - write, testing, coding craft, implementation of method correlated with one another
    - class[constructor], string[formatting, slicing], sorted(), comprehension[list, gen]

- 05th December - `Edabit` - FreeRange - `VeryHard`
    - 5mins - understanding questions, decomposed strategy
    - 20mins - write, testing, coding craft, 
    - string[formatting, slicing], unpack[tuple, variable], list[forloops, .append()]

- 05th December - `Edabit` - GrantTheHint - `VeryHard`
    - 15mins - understanding questions, decomposed strategy
    - 45mins - write, testing, coding craft, implementing nested ''.join, about to give up 
    - string[formatting, slicing], comprehension(''.join nested, generator), len(), max(), range()

- 06th December - `Edabit` - MultiChoiceTests - `VeryHard`
    - 20mins - understanding questions, understanding classes scope
    - 50mins - write, testing, coding craft, implementation of scope, modifying attributes
    - class[constructor, local scope, args], dict[update], comprehension[dict, gen], one-liner[conditionals]

- [UNSOLVED] - 07th December - `Edabit` - IsParallelogram - `VeryHard`
    - 20mins - understanding questions, understanding method to check validity of cases
    - 25mins - write, testing, coding craft, 
    - list, map(), math[sqrt], lambda

- [UNSOLVED] - 07th December - `Edabit` - MagicNumber - `VeryHard`
    - 45mins - understanding questions, understanding tilde operator
    - 45mins - write, testing, coding craft, implementing tilde to slice counter diagonal
    - tilde(~)

- 07th December - `Edabit` - TicTacToe - `VeryHard`
    - 10mins - understanding questions, strategizing logic
    - 20mins - write, testing, coding craft, implementing tilde to slice counter diagonal, improvise result
    - tilde(~), set(), comprehension[list, generator, ''.join()], one-liner(conditionals)

- 07th December - `Edabit` - TicTacToe2 - `VeryHard`
    - 3mins - understanding questions, similar with previous TicTacToe
    - 10mins - write, testing, coding craft, implementing tilde to slice counter diagonal, tinker to suit tests
    - tilde(~), set(), comprehension[list, generator, ''.join(), zip()], one-liner(conditionals)

- 08th December - `Edabit` - TicTacToe3 - `VeryHard`
    - 3mins - understanding questions, similar with previous TicTacToe
    - 6mins - write, testing, coding craft, implementing tilde to slice counter diagonal, tinker to suit tests
    - tilde(~), set(), comprehension[list, generator, ''.join(), zip()], one-liner(conditionals)

- 08th December - `Edabit` - CaesarCipher - `VeryHard`
    - 5mins - understanding questions, decomposed strategy, learning about shifting characters
    - 15mins - write, testing, coding craft, implementing shifting character using modulus 
    - string[''.join(), chr(), ord(), isalpha(), .islower(), .isupper()]

- 09th December - `Edabit` - OddEvenPosFrLast - `VeryHard`
    - 15mins - understanding questions, decomposed strategy, reverse ranging
    - 15mins - write, testing, coding craft, implementing reverse ranging 
    - string[reverse by [::-1], reverse range], one-liner[conditionals]

- [UNSOLVED] - 09th December - `Edabit` - ShiftLetters - `VeryHard`
    - 30mins - understanding questions, decomposed strategy, reverse ranging
    - 60mins - write, testing, coding craft, implementing reverse ranging 
    - 

- 09th December - `Edabit` - BingoCheck - `VeryHard`
    - 5mins - understanding questions, decomposed strategy
    - 5mins - write, testing, coding craft, implementing diagonals, c-diag, rows cols operation 
    - matrix[rows, cols, diag, c-diag], boolean[any, or], comprehension[gen], set(), len()

- 09th December - `Edabit` - HashJuggling - `Expert`
    - 10mins - understanding questions, strategizing
    - 10mins - write, testing, coding craft, implementing sha256, reorder according to alpha, then digit
    - crypto[sha256, hashlib, hexdigest(), encode()], comprehension[gen, list], string[isalpha(), isdigit()]

- 10th December - `Edabit` - FractToMixed - `Expert`
    - 15mins - understanding questions, strategizing application of divmod and fractions
    - 30mins - write, testing, coding craft, implementing fractions, divmod
    - fractions[Fraction], divmod(), string[.format(), slicing, split()], one-liner[conditionals]

- 10th December - `Edabit` - EdabitEncryption - `Expert`
    - 10mins - understanding questions, strategizing application of nested slicing
    - 15mins - write, testing, coding craft, implementing nested slicing string
    - math[sqrt(), round()], string[''.join(), split()], len, one-liner[conditionals(on var assgn), list]

- 11th December - `Edabit` - TripletMaxMinProd - `VeryHard`
    - 10mins - understanding questions, decomposed strategy, understanding itertools combinations
    - 5mins - write, testing, coding craft 
    - itertools[combinations], functools[reduce], comprehension[gen], max(), min(), lambda

- 11th December - `Edabit` - CramerSimLinEq - `Expert`
    - 20mins - understanding questions, strategizing application of Cramer's for simultaneous linear equations
    - 10mins - write, testing, coding craft, implementing determinant, Cramer's method
    - math[Matrix, Cramer,s, algebra], var assgn, tuple unpacking, boolean

- 12th December - `Edabit` - FootballTourney - `Expert`
    - 60mins - 1st method, unsuccessfull implementation of dict data structure, too complicated
    - 35mins - understanding questions, strategizing data structure and sorting
    - 35mins - write, testing, coding craft, implementing on fly calculation, sorting by multiple columns
    - DS[list, dict], for loops, tuple unpacking, slicing[list], dict.get(), comprehension[gen, list, dict]

- 12th December - `Edabit` - ExtPrimeOccFromInt - `Expert`
    - 20mins - understanding questions, strategizing data structure and placeholders, getting combinations
    - 30mins - write, testing, coding craft, processing processed nums, logic implementation
    - DS[list], slicing[list], comprehension[gen, nested list], range(), len(), bool()

- 13th December - `Edabit` - NumThenLett - `Expert`
    - 20mins - understanding questions, strategizing data structure and different size of nested
    - 30mins - write, testing, coding craft, implementation for logic in dividing a list to nested
    - DS[list], slicing[list], comprehension[gen, nested list], range(), len(), isinstance(), sorted()

- 13th December - `Edabit` - ASCIIP2BarChart - `Expert`
    - 30mins - understanding questions, decomposed strategy, reverse ranging
    - 60mins - write, testing, coding craft, implementing reverse ranging 
    - 35mins - passed on 15thDecember after careful implementation of tuple, flow of data
    - DS[set, tuple, nested list], comprehension[list, nested], string[.format(), .join()], sorted()

- 14th December - `Edabit` - DecryptDigTolett - `Expert`
    - 10mins - understanding questions, decomposed strategy, reverse ranging
    - 50mins - write, testing, coding craft, implementing reverse ranging 
    - 30mins - 15th December - epiphany on using replace function
    - DS[list, dict], string[replace(), slicing], for loops

- 14th December - `Edabit` - MTGManaCost - `Expert`
    - 10mins - understanding questions, decomposed strategy, dataflow planning
    - 50mins - write, testing, coding craft, testing the implementation
    - 30mins - solved on 17thDecember 2020 after careful planning
    - DS[list], for loops, string[formatting, ''join()], one-liner[conds, bool, var assgnment]

- 14th December - `Edabit` - ParenthesesClus - `VeryHard`
    - 5mins - understanding questions, decomposed strategy
    - 25mins - write, testing, coding craft, implementing counter for loops, conditional 
    - bottom up approach, for loops w counter, comprehension[list]

- 15th December - `Edabit` - ActualUSBFlashSize - `Expert`
    - 20mins - understanding questions, strategizing data structure
    - 30mins - write, testing, coding craft, refactor code
    - DS[list, string], slicing[list], comprehension[gen, list], string[''.join(), .format()]

- 16th December - `Edabit` - ISBN13andISBN10 - `Expert`
    - 30mins - understanding questions, strategizing data structure, startegixing conds
    - 35mins - write, testing, coding craft, implementing various conds for challenge
    - DS[list, string], conds, slicing[list, string], comprehension[gen, list], string[''.join()]

- 17th December - `Edabit` - TruncPrimes - `Expert`
    - 15mins - understanding questions, strategizing data structure, startegizing conds, prime testing
    - 45mins - write, testing, coding craft, implementing various conds, prime testing, comprehension for challenge
    - Math[Primality, number theory], DS[list, string], conds[one-liner, bool], slicing[string], comprehension[list]

- [UNSOLVED] - 17th December - `Edabit` - Longest non repeating - `VeryHard`
    - 45mins - understanding questions, decomposed strategy, reverse ranging
    - 45mins - write, testing, coding craft, implementing reverse ranging 
    - 

- [NOTSUBMITTED] - 18th December - `Edabit` - IsItTwoPrimesProduct - `Expert`
    - 30mins - understanding questions, strategizing data structure, prime testing, sieve of erastothenes
    - 25mins - write, testing, coding craft, implementing sieve of erastothenes, prime testing
    - Math[Primality, number theory], DS[list, string], conds[one-liner, bool], comprehension[list]

- [NOTSUBMITTED] - 20th December - `Edabit` - MatTranspose - `Hard`
    - 3mins - understanding questions, strategizing data structure
    - 5mins - write, testing, coding craft, implementing zip
    - Math[Matrix], zip(), comprehension[list]

- [NOTSUBMITTED] - 23rd December - `Edabit` - KeywordCipher - `Expert`
    - 30mins - understanding questions, strategizing data structure, set strategizing
    - 25mins - write, testing, coding craft, implementing ds, copying test cases
    - DS[list, string, set], one-liner[var assgn], comprehension[list, ''.join(), gen], for loops

- [NOTSUBMITTED] - 24th December - `Edabit` - BugJumpInParabollic - `Expert`
    - 60mins - understanding questions, strategizing data structure, strategizing parabolic formula
    - 50mins - write, testing, coding craft, implementing parabolic, copying test cases, testing implementation
    - Math[parabolic function, sqrt], one-liner[var assgn, boolean]

- [NOTSUBMITTED] - 25th December - `Edabit` - CountRugLayers - `Hard`
    - 5mins - understanding questions, strategizing data structure
    - 10mins - write, testing, coding craft, copying testcases, implementing loops
    - for loops, slicing[list], counter

- [NOTSUBMITTED] - 26th December - `Edabit` - Mat90DegClockwise - `Hard`
    - 5mins - understanding questions, strategizing data structure
    - 10mins - write, testing, coding craft, copying testcases, implementing matrix transpose
    - zip, slicing[list], comprehension

- [NOTSUBMITTED] - 27th December - `Edabit` - MatTranspose - `Hard`
    - 5mins - understanding questions, strategizing data structure
    - 10mins - write, testing, coding craft, copying testcases, implementing matrix transpose
    - zip, comprehension

- [NOTSUBMITTED] - 28th December - `Edabit` - FirstBeforeSecond - `Hard`
    - 5mins - understanding questions, strategizing data structure
    - 10mins - write, testing, coding craft, copying testcases, implementing boolean
    - comprehension[list], sorted(key=index), boolean

- [NOTSUBMITTED] - 29th December - `Edabit` - OddEvenPosit - `Hard`
    - 5mins - understanding questions, strategizing data structure
    - 10mins - write, testing, coding craft, copying testcases, implementing boolean
    - comprehension[list, gen], one-liner(conds), ''.join(), slicing[list, string]

- 29th December - `Edabit` - DistToNearestVowel - `VeryHard`
    - 10mins - understanding questions, strategizing data structure
    - 15mins - write, testing, coding craft, copying testcases, implementing tuple
    - comprehension[list, gen, tuple], min(), abs(), slicing[tuple]

- [NOTSUBMITTED] - 30th December - `Edabit` - PlaybackDuration - `Hard`
    - 10mins - understanding questions, strategizing data structure, strategizing strings
    - 10mins - write, testing, coding craft, copying testcases, implementing logic for clock
    - Math[modulus, clock], comprehension[list, gen], ''.join(), slicing[string]

- [NOTSUBMITTED] - 31st December - `Edabit` - BoxMaking - `Hard`
    - 10mins - understanding questions, strategizing data structure, strategizing implement
    - 10mins - write, testing, coding craft, copying testcases, implementing pattern printing
    - Pattern Printing, comprehension[nested list, gen], ''.join(), conds