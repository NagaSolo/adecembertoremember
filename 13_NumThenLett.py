''' Challenge: Numbers First, Letters Second [NumThenLett]

Published by:skrzyniarzp

Summary:
  Write a function that sorts list while keeping the list structure. 
  Numbers should be first then letters both in ascending order.

Examples:
  num_then_char([
    [1, 2, 4, 3, "a", "b"],
    [6, "c", 5], [7, "d"],
    ["f", "e", 8]
  ]) ➞ [
    [1, 2, 3, 4, 5, 6],
    [7, 8, "a"],
    ["b", "c"], ["d", "e", "f"]
  ]

  num_then_char([
    [1, 2, 4.4, "f", "a", "b"],
    [0], [0.5, "d","X",3,"s"],
    ["f", "e", 8],
    ["p","Y","Z"],
    [12,18]
  ]) ➞ [
    [0, 0.5, 1, 2, 3, 4.4],
    [8],
    [12, 18, "X", "Y", "Z"],
    ["a", "b", "d"],
    ["e", "f", "f"],
    ["p", "s"]
  ]

Notes:
  Test cases will contain integer and float numbers and single letters.

'''

def num_then_char(lst):
    lgt = [len(i) for i in lst]
    rge = [sum(lgt[0:i]) for i in range(len(lgt)+1)]
    f_lst = [i for i in lst for i in i]
    a_lst = sorted([i for i in f_lst if isinstance(i, int) or isinstance(i, float)]) + sorted([i for i in f_lst if isinstance(i, str)])
    return [a_lst[i:j] for i,j in zip(rge, rge[1:])]

if __name__ == "__main__":
    tests = [
        num_then_char([[1, 2, 4, 3, "a", "b"],[6, "c", 5],[7, "d"],["f", "e", 8]]), 
        # [[1, 2, 3, 4, 5, 6], [7, 8, 'a'], ['b', 'c'], ['d', 'e', 'f']]
        num_then_char([
            [1, 2, 4.4, "f", "a", "b"],
            [0],
            [0.5, "d","X",3,"s"],
            ["f", "e", 8],
            ["p","Y","Z"],
            [12,18]]), 
        # [[0, 0.5, 1, 2, 3, 4.4], [8], [12, 18, 'X', 'Y', 'Z'], ['a', 'b', 'd'], ['e', 'f', 'f'], ['p', 's']]
    ]
    for t in tests:
        print(t)