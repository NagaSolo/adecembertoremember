''' Challenge: Oddly or Evenly Positioned [OddEvenPosit]
Published by Deep Xavier

Summary:
    Create a function that returns the characters from a list or string r on odd or even positions, 
    depending on the specifier s. 
    
    The specifier will be: 
        "odd" for items on odd positions (1, 3, 5, ...) 
        and "even" for items on even positions (2, 4, 6, ...).

Examples:
    char_at_pos([2, 4, 6, 8, 10], "even") 
        ➞ [4, 8]
    # 4 & 8 occupy the 2nd & 4th positions

    char_at_pos("EDABIT", "odd") 
        ➞ "EAI"
    # "E", "A" and "I" occupy the 1st, 3rd and 5th positions

    char_at_pos(["A", "R", "B", "I", "T", "R", "A", "R", "I", "L", "Y"], "odd") 
        ➞ ["A", "B", "T", "A", "I", "Y"]

Notes:
    Lists are zero-indexed, so, index+1 = position or position-1 = index.

    There will not be an empty string or an empty list.

    (Optional) Try solving this challenge in a single-line lambda function.

    A more advanced version of this challenge can be found here.

'''

def char_at_pos(r, s):
    res = [i for i in r[1:len(r):2]] if s == 'even' else [i for i in r[:len(r):2]]
    return ''.join(res) if isinstance(r, str) else res

if __name__ == "__main__":
    tests = [
        char_at_pos([2, 4, 6, 8, 10], "even"), # [4, 8]
        char_at_pos([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "odd"), # [1, 3, 5, 7, 9]
        char_at_pos("EDABIT", "even"), # "DBT"
        char_at_pos("EDABIT", "odd"), # "EAI"
        char_at_pos("QWERTYUIOP", "even"), # "WRYIP"
        char_at_pos("POIUYTREWQ", "odd"), # "PIYRW"
        char_at_pos("ASDFGHJKLZ", "odd"), # "ADGJL"
        char_at_pos("ASDFGHJKLZ", "even"), # "SFHKZ"
        char_at_pos(["!", "@", "#", "$", "%", "^", "&", "*", "(", ")"], "odd"), # ["!", "#", "%", "&", "("]
        char_at_pos([")", "(", "*", "&", "^", "%", "$", "#", "@", "!"], "odd"), # [")", "*", "^", "$", "@"]
        char_at_pos(["A", "R", "B", "I", "T", "R", "A", "R", "I", "L", "Y"], "odd"), # ["A", "B", "T", "A", "I", "Y"]
    ]
    for t in tests:
        print(t)