''' Challenge: Fruit Smoothie [FruitSmoothie]

Published by: Joshua Senoron

Summary:
    Create a class Smoothie and do the following:

        Create an instance attribute called ingredients.

        Create a get_cost method which calculates the total cost of the ingredients used to make the smoothie.

        Create a get_price method which returns the number from get_cost plus the number from get_cost multiplied by 1.5. 
        Round to two decimal places.
        
        Create a get_name method which gets the ingredients and puts them in 
        alphabetical order into a nice descriptive sentence. 
        If there are multiple ingredients, 
        add the word "Fusion" to the end but otherwise, 
        add "Smoothie". 
        Remember to change "-berries" to "-berry". See the examples below.

        Ingredient	    Price
        Strawberries	$1.50
        Banana	        $0.50
        Mango	        $2.50
        Blueberries	    $1.00
        Raspberries	    $1.00
        Apple	        $1.75
        Pineapple	    $3.50

Examples:
    s1 = Smoothie(["Banana"])

    s1.ingredients ➞ ["Banana"]

    s1.get_cost() ➞ "$0.50"

    s1.get_price() ➞ "$1.25"

    s1.get_name() ➞ "Banana Smoothie"
    
    s2 = Smoothie(["Raspberries", "Strawberries", "Blueberries"])

    s2.ingredients ➞ ["Raspberries", "Strawberries", "Blueberries"]

    s2.get_cost() ➞ "$3.50"

    s2.get_price() ➞ "$8.75"

    s2.get_name() ➞ "Blueberry Raspberry Strawberry Fusion"

Notes:
    Feel free to check out the Resources tab for a refresher on classes.

'''

class Smoothie:
    def __init__(self, ingredients):
        self.ingredients = ingredients
        self.prices = {
            "Strawberries" : "$1.50","Banana" : "$0.50",
            "Mango" : "$2.50","Blueberries" : "$1.00",
            "Raspberries" : "$1.00",
            "Apple" : "$1.75","Pineapple" : "$3.50"
        }
    
    def get_cost(self):
        return '${:.2f}'.format(sum(float(self.prices[i][1:]) for i in self.ingredients))

    def get_price(self):
        cost = sum(float(self.prices[i][1:]) for i in self.ingredients)
        return '${:.2f}'.format(cost + (cost*1.5))

    def get_name(self):
        if len(self.ingredients) > 1:
            return ' '.join(i if i[-1]!='s' else i[:-3]+'y' for i in sorted(self.ingredients)) + ' Fusion'
        return ' '.join(i if i[-1]!='s' else i[:-3]+'y' for i in self.ingredients) + ' Smoothie'

if __name__ == "__main__":
    s1 = Smoothie(["Banana"])
    s2 = Smoothie(["Raspberries", "Strawberries", "Blueberries"])
    s3 = Smoothie(["Mango", "Apple", "Pineapple"])
    s4 = Smoothie(["Pineapple", "Strawberries", "Blueberries", "Mango"])
    s5 = Smoothie(["Blueberries"])

    tests = [
        s1.ingredients, # ["Banana"]
        s1.get_cost(), # "$0.50"
        s1.get_price(), # "$1.25"
        s1.get_name(), # "Banana Smoothie"
        s2.ingredients, # ["Raspberries", "Strawberries", "Blueberries"]
        s2.get_name(), # "Blueberry Raspberry Strawberry Fusion"
    ]
    for t in tests:
        print(t)