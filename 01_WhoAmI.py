''' Challenge: Wait... Who Am I? [WhoAmI]

Published by: MegaLoser

Summary:
    Hello there, 
    I... seem to not remember who I am, 
    my memories is all... cloudy, 
    although maybe if I could piece it together...

    Oh! 
    Maybe you could help me make a class that helps me remember things. 
    
    Maybe a method called add that would add to my memory if 
    I would recall things and a remember method that would let me recall a specific memory.

    But you have to make add more flexible, 
    I might recall many things in an instant or only one that I would forget again.

Examples:
    # add method doesn't return anything.
    memories.add(name="Shane", gender="M", catch_phrase="bazinga")

    memories.add(work="None", love_life=0)

    memories.add(adress="car")

    memories.remember("work") 
        ➞ "None"

    memories.remember("gender") 
        ➞ "M"

    # If memory was not added, return False
    memories.remember("lover") 
        ➞ False

Notes:
    The add method should be able to handle any number of arguments.

'''

class Memories:
    def __init__(self):
        self.mems = {}
    def add(self, **kwargs):
        for k, v in kwargs.items():
            self.mems.update({k:v})
    def remember(self, key):
        return self.mems.get(key)

if __name__ == "__main__":
    memories = Memories()
    memories.add(purpose="lol idk", social_life="What's that?", dream="money")
    memories.add(fears="code", languages=["english", "filipino", "python", "Java"])
    memories.add(favorite_number=404, is_gamer=True, race="Filipino", gender="M", name="Shane", embarassing_moments="dance with her.. oh god", omai_wa_mo_shindeiru="NANI?!")
    memories.add(strength_level="Over 9000!!!")
    
    tests = [
        memories.remember("fears"), # "code"
        memories.remember("race"), # "Filipino"
        memories.remember("dream"), # "money"
    ]

    for t in tests:
        print(t)