''' Challenge: Multiple Choice Tests [MultiChoiceTests]

Published by: Joshua Senoron

Summary:
    Your task is to write a program 
    which allows teachers to create a multiple choice test in a class called Testpaper 
    and to be also able to assign a minimum pass mark. 
    
    The testpaper's subject should also be included. 
    
    The attributes are in the following order:

        subject
        markscheme
        pass_mark

    As well as that, 
    we need to create student objects to take the test itself! 
    
    Create another class called Student and do the following:

        Create an attribute called tests_taken and set the default as 'No tests taken'.

        Make a method called take_test(), 
        which takes in the testpaper object they are completing and the student's answers. 
        
        Compare what they wrote to the mark scheme, 
        and append to the/create a dictionary assigned to tests_taken in the way as shown in the point below.
        
        Each key in the dictionary should be the testpaper subject 
        and each value should be a string in the format seen in the examples below 
        (whether or not the student has failed, and their percentage in brackets).

Examples:
    paper1 = Testpaper("Maths", ["1A", "2C", "3D", "4A", "5A"], "60%")
    paper2 = Testpaper("Chemistry", ["1C", "2C", "3D", "4A"], "75%")
    paper3 = Testpaper("Computing", ["1D", "2C", "3C", "4B", "5D", "6C", "7A"], "75%")

    student1 = Student()
    student2 = Student()
    student1.tests_taken ➞ "No tests taken"
    student1.take_test(paper1, ["1A", "2D", "3D", "4A", "5A"])
    student1.tests_taken ➞ {"Maths" : "Passed! (80%)"}

    student2.take_test(paper2, ["1C", "2D", "3A", "4C"])
    student2.take_test(paper3, ["1A", "2C", "3A", "4C", "5D", "6C", "7B"])
    student2.tests_taken ➞ {"Chemistry" : "Failed! (25%)", "Computing" : "Failed! (43%)"}

Notes:
    Round percentages to the nearest whole number.

    Remember that the attribute tests_taken should return 'No tests taken' when no tests have been taken yet.

'''

class Testpaper:
    def __init__(self, *args):
        self.subject = args[0]
        self.markscheme = args[1]
        self.pass_mark = args[2]

class Student():
    def __init__(self):
        self.tests_taken = 'No tests taken'
        self.taken = {}
    def take_test(self, *args):
        sub, markscheme, pass_mark, ans = args[0].subject, args[0].markscheme, int(args[0].pass_mark[:-1]), args[1]
        score = (sum(1 for i in range(len(ans)) if ans[i] == markscheme[i])/len(ans))*100
        grade = 'Passed! ({}%)'.format(round(score)) if score >= pass_mark else 'Failed! ({}%)'.format(round(score))
        self.taken.update({sub : grade})
        self.tests_taken = self.taken
        
if __name__ == "__main__":
    paper1 = Testpaper('Maths', ['1A', '2C', '3D', '4A', '5A'], '60%')
    paper2 = Testpaper('Chemistry', ['1C', '2C', '3D', '4A'], '75%')
    paper3 = Testpaper('Computing', ['1D', '2C', '3C', '4B', '5D', '6C', '7A'], '75%')
    paper4 = Testpaper('Physics', ['1A', '2B', '3A', '4C', '5A', '6C', '7A', '8C', '9D', '10A', '11A'], '90%')
    
    student1 = Student()
    student2 = Student()
    student3 = Student()
    
    print(student1.tests_taken), # 'No tests taken'
    student1.take_test(paper1, ['1A', '2D', '3D', '4A', '5A']),
    print(student1.tests_taken), # {'Maths' : 'Passed! (80%)'}

    student2.take_test(paper2, ['1C', '2D', '3A', '4C'])
    student2.take_test(paper3, ['1A', '2C', '3A', '4C', '5D', '6C', '7B'])
    print(student2.tests_taken), # {'Chemistry' : 'Failed! (25%)', 'Computing' : 'Failed! (43%)'}