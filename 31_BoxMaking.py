''' Challenge: Making a Box [BoxMaking]

Published by: Helen Yu

Summary:
    Create a function that creates a box based on dimension n.

Examples:
    make_box(5) 
        ➞ [
            "#####",
            "#   #",
            "#   #",
            "#   #",
            "#####"
        ]

    make_box(3) 
        ➞ [
            "###",
            "# #",
            "###"
        ]

    make_box(2) 
        ➞ [
            "##",
            "##"
        ]

    make_box(1) 
        ➞ [
            "#"
        ]

'''

def make_box(n):
    return [''.join('#' if i == 0 or i == n-1 or j==0 or j == n-1 else ' ' for i in range(n)) for j in range(n)]

if __name__ == "__main__":
    tests = [
        make_box(5), # ["#####", "#   #", "#   #", "#   #", "#####"]

        make_box(6), # ["######", "#    #", "#    #", "#    #", "#    #", "######"]

        make_box(4), # ["####", "#  #", "#  #", "####"]

        make_box(2), # ["##", "##"]

        make_box(1), # ["#"]
    ]

    for t in tests:
        print(t)