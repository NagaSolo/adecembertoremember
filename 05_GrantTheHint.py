''' Challenge: Give Me a Hint [GrantTheHint]

Published by: Joshua Senoron

Summary:
    Given a sentence, 
    return a list of strings which gradually reveals the next letter in every word at the same time. 
    
    Use underscores to hide the remaining letters.

Examples
grant_the_hint("Mary Queen of Scots") ➞ [
  "____ _____ __ _____",
  "M___ Q____ o_ S____",
  "Ma__ Qu___ of Sc___",
  "Mar_ Que__ of Sco__",
  "Mary Quee_ of Scot_",
  "Mary Queen of Scots"
]

grant_the_hint("The Life of Pi") ➞ [
  "___ ____ __ __",
  "T__ L___ o_ P_",
  "Th_ Li__ of Pi",
  "The Lif_ of Pi",
  "The Life of Pi"
]
Notes
Maintain capitalisation.

'''

def grant_the_hint(txt):
    mx = max(len(w) for w in txt.split(' '))
    return [' '.join(''.join((w[:i]+'_'*(mx-i))[:len(w)]) for w in txt.split(' ')) for i in range(mx+1)]

if __name__ == "__main__":
    tests = [
        grant_the_hint('Mary Queen of Scots'), 
        # [
        #     '____ _____ __ _____','M___ Q____ o_ S____',
        #     'Ma__ Qu___ of Sc___','Mar_ Que__ of Sco__',
        #     'Mary Quee_ of Scot_','Mary Queen of Scots'
        # ]
    ]
    for t in tests:
        print(t)