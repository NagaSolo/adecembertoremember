''' Challenge: Keyword Cipher [KeywordCipher]

Published by: Mubashir Hassan

Summary:
    A Keyword Cipher is a monoalphabetic cipher,
    which uses a keyword to provide encryption on given string of message.

    Create a function that takes two arguments; 
        a string of message and a string of key and returns an encoded message.

    There are some variations on the rules of encipherment. 
    
    One version of the cipher rules are outlined below:

        message = "ABCHIJ"
        key = "KEYWORD"

        keyword_cipher(key, message) 
            ➞ "KEYABC"
        
        Write all alphabets from A to Z:
        A	B	C	D	E	F	G	H	I	J	K	L	M	N	O	P	Q	R	S	T	U	V	W	X	Y	Z
        
        All alphabets which are part of keyword are removed and the alphabets are re-arranged such that keyword appears first, 
        followed by the rest of the alphabets in usual order.
        K	E	Y	W	O	R	D	A	B	C	F	G	H	I	J	L	M	N	P	Q	S	T	U	V	X	Z
        
        Return all alphabets of key against given message:
        eMessage = "KEYABC"
        // A = K, B = E, C = Y, H = A, I = B, J = C

Examples:
    keyword_cipher("keyword", "abchij") 
        ➞ "keyabc"

    keyword_cipher("purplepineapple", "abc") 
        ➞ "pur"

    keyword_cipher("mubashir", "edabit") 
        ➞ "samucq"

Notes:
    Don't forget to remove duplicates after concatenating string with keyword.
    
    Take care of characters other than alphabets.

'''

def keyword_cipher(key, message):
    from string import ascii_lowercase
    dup_k, new_k = set(), ''
    for i in key:
        if i not in dup_k:
            dup_k.add(i)
            new_k = new_k+i
    str_proc = ''.join(c for c in ascii_lowercase if c not in new_k)
    k_w_ascii = key if len(str_proc) == 0 else new_k+str_proc
    return ''.join(k_w_ascii[ascii_lowercase.index(i)] if i in ascii_lowercase else i for i in message)

if __name__ == "__main__":
    tests = [
        keyword_cipher("keyword","abc"), #"key"
        keyword_cipher("keyword","xyz"), #"vxz",
        keyword_cipher("keyword","aeiou"), # "kobjs"
        keyword_cipher("purplepineapple","abc"), # "pur"
        keyword_cipher("purplepineapple","xyz"), # "xyz"
        keyword_cipher("purplepineapple","aeiou"), # "pebjt"
        keyword_cipher("etaoinshrdlucmfwypvbgkjqxz","abc"), # "eta"
        keyword_cipher("etaoinshrdlucmfwypvbgkjqxz","xyz"), # "qxz"
        keyword_cipher("etaoinshrdlucmfwypvbgkjqxz","aeiou"), # "eirfg"
        keyword_cipher("tfozcivbsjhengarudlkpwqxmy","abc"), # "tfo"
        keyword_cipher("tfozcivbsjhengarudlkpwqxmy","tjuukf"), # "kjpphi"
        keyword_cipher("tfozcivbsjhengarudlkpwqxmy","ajqqtb"), # "tjuukf"
        keyword_cipher("tfozcivbsjhengarudlkpwqxmy","a_bc&*83"), # "t_fo&*83"
        keyword_cipher("mubashir","pakistan"), # "lmecpqmj"
        keyword_cipher("mubashir","edabit"), # "samucq"
    ]
    for t in tests:
        print(t)