''' Challenge: Product of Two Prime Numbers [IsItTwoPrimesProduct]

Published by: llllll
Write a function that returns True if the given number num is a product of any two prime numbers.

Examples
product_of_primes(2059) ➞ True
# 29*71=2059

product_of_primes(10) ➞ True
# 2*5=10

product_of_primes(25) ➞ True
# 5*5=25

product_of_primes(999) ➞ False
# There are no prime numbers.
Notes
num is always greater than 0.
0 and 1 aren't prime numbers.

'''

def sieve(n):
    numbers = set(range(2, n))
    for i in range(2, int(n ** 0.5) + 1):
        for j in range(i * 2, n, i):
            numbers.discard(j)
    return sorted(numbers)

def product_of_primes(num):
    from itertools import combinations
    lst = sieve(num)
    return any(i*j == num for i in lst for j in lst)

if __name__ == "__main__":
    tests = [
        product_of_primes(2059), # True
        product_of_primes(7), # False
        product_of_primes(25), # True
        product_of_primes(39), # True
        product_of_primes(5767), # True
        product_of_primes(77), # True
        product_of_primes(12), # False
        product_of_primes(8), # False
    ]
    for t in tests:
        print(t)