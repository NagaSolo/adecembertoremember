''' Challenge: Parabolic Jumps [BugJumpInParabollic]

Published by: MyName

Summary:
    When a bug jumps to a height y, 
    its position can be plotted as a quadratic function with x as the time, 
    and y as the vertical distance traveled. 
    
    For example, 
    for a bug that jumps 9 cm up and is back on the ground after 6 seconds, 
    its trajectory can be plotted as:

        bug jump quadratic plot

    Create a function that, 
    given the max height of a vertical jump in cm and the current time in milliseconds, 
    returns the current position of the bug rounded to two decimal places, 
    and its direction ("up" or "down"). 
    
    If the bug is already back on the ground, 
    return 0 for the position and None for the direction.

Examples:
    bug_jump(9, 1000) 
        ➞ [5, "up"]

    bug_jump(9, 4000) 
        ➞ [8, "down"]

    bug_jump(9, 5500) 
        ➞ [2.75, "down"]

    bug_jump(9, 7000) 
        ➞ [0, None]

Notes:
    Time and position both start at 0.
    
    You only need to translate the parabola (you don't need to scale it).

'''

def bug_jump(height, time):
    # eq is x**2 - b*x
    from math import sqrt
    ver_t, tm_s = sqrt(height), time/1000
    lan_t = ver_t*2
    up, down = tm_s < ver_t, tm_s > ver_t and tm_s < lan_t
    traje = 'up' if up else 'down' if down else None
    posit = '{:.2f}'.format(tm_s*(ver_t+(ver_t - tm_s))) if up or down else 0
    return posit, traje

if __name__ == "__main__":
    tests = [
        bug_jump(9,1000), # [5, "up"]
        bug_jump(9,4000), # [8, "down"]
        bug_jump(9,5500), # [2.75, "down"]
        bug_jump(9, 7000), # [0, None]
        bug_jump(5,2230), # [5, "up"]
        bug_jump(5,2240), # [5, "down"]
        bug_jump(18,8485), # [0, "down"]
        bug_jump(18,8490), # [0, None]
        bug_jump(12,1500), # [8.14, "up"]
        bug_jump(3,2650), # [2.16, "down"]
    ]

    for t in tests:
        print(t)