''' Challenge: Oddly or Evenly Positioned From Last [OddEvenPosFrLast]

Published by: Deep Xavier

Summary:
    Create a function that extracts the characters from a list (or a string) on odd or even positions, 
    depending on the specifier. 
    
    The string "odd" for items on: 
        odd positions (... 5, 3, 1) and 
        "even" for even positions (... 6, 4, 2) 
        from the last item of that list or string.

Examples:
    char_at_pos([2, 4, 6, 8, 10], "even") 
        ➞ [4, 8]
    # 4 & 8 occupy the 4th & 2nd positions from right.

    char_at_pos("EDABIT", "odd") 
        ➞ "DBT"
    # "D", "B" and "T" occupy the 5th, 3rd and 1st positions from right.

    char_at_pos([")", "(", "*", "&", "^", "%", "$", "#", "@", "!"], "odd") 
        ➞ ["(", "&", "%", "#", "!"]

Notes:
    Lists are zero-indexed, 
    so, 
    index+1 = position or position-1 = index.
    
    Optional: Solve this challenge in a single-line lambda function.

'''

def char_at_pos(r, s):
    return r[-2:-len(r)-1:-2][::-1] if s == 'even' else r[-1:-len(r)-1:-2][::-1]

if __name__ == "__main__":
    tests = [
        char_at_pos("EDABIT", "even"), # "EAI"
        char_at_pos("EDABIT", "odd"), # "DBT"
        char_at_pos("QWERTYUIOP", "even"), # "QETUO"
        char_at_pos("POIUYTREWQ", "odd"), # "OUTEQ"
        char_at_pos([2, 4, 6, 8, 10], "even"), # [4, 8]
        char_at_pos(["!", "@", "#", "$", "%", "^", "&", "*", "(", ")"], "odd"), # ["@", "$", "^", "*", ")"]
    ]
    for t in tests:
        print(t)