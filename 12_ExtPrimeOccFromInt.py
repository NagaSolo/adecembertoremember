''' Challenge: Find All Prime Numbers in Decimal Integer [ExtPrimeOccFromInt]

Published by: ChrisCrossCrash

Summary:
    Create a function that takes an integer argument 
    and 
    returns a list of prime numbers found in the decimal representation of that number (not factors).

    For example, 
    extract_primes(1717) returns [7, 7, 17, 17, 71].

    The list should be in acending order. 
    
    If a prime number appears more than once, 
    every occurance should be listed. 
    
    If no prime numbers are found, 
    return an empty list.

Examples:
    extract_primes(1) ➞ []

    extract_primes(7) ➞ [7]

    extract_primes(73) ➞ [3, 7, 73]

    extract_primes(103) ➞ [3]

    extract_primes(1313) ➞ [3, 3, 13, 13, 31, 131, 313]

Notes:
    All test cases are positive real integers.
    
    Some numbers will have leading zeros. 
    
    For example, 
    the number 103 contains the prime number 3, 
    but also contains 03. 
    
    These should be treated as the same number, 
    so the result would simply be [3].

'''

def extract_primes(num):
    if num < 11 and num in [2,3,5,7]:
        return [num]
    elif num is 1:
        return []
    num = str(num)
    comb = [[num[i:len(num)-j] for j in range(len(num))] for i in range(len(num))]
    nums = [int(i) for i in comb for i in i if (i.isdigit() and int(i) not in [0, 1] and i[0] != '0')]
    b_pri = [all([True if n%i != 0 else False for i in range(2,n)]) for n in nums]
    return sorted([nums[i] for i in range(len(nums)) if b_pri[i] == True])

if __name__ == "__main__":
    tests = [
        extract_primes(1), # []
        extract_primes(2), # [2]
        extract_primes(3), # [3]
        extract_primes(13), # [3, 13]
        extract_primes(101), # [101]
        extract_primes(313), # [3, 3, 13, 31, 313]
        extract_primes(10234), # [2, 3, 23]
    ]
    for t in tests:
        print(t)