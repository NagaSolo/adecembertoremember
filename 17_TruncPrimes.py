''' Challenge: Truncatable Primes [TruncPrimes]

Published by: 02matwil

Summary:
    A left-truncatable prime is a prime number that contains no 0 digits and, 
    when the first digit is successively removed, 
    the result is always prime.

    A right-truncatable prime is a prime number that contains no 0 digits and, 
    when the last digit is successively removed, 
    the result is always prime.

    Create a function that takes an integer as an argument and:

        If the integer is only a left-truncatable prime, 
        return "left".
        
        If the integer is only a right-truncatable prime, 
        return "right".
        
        If the integer is both, return "both".
        
        Otherwise, 
        return False.

Examples:
    truncatable(9137) 
        ➞ "left"
    # Because 9137, 137, 37 and 7 are all prime.

    truncatable(5939) 
        ➞ "right"
    # Because 5939, 593, 59 and 5 are all prime.

    truncatable(317) 
        ➞ "both"
    # Because 317, 17 and 7 are all prime and 317, 31 and 3 are all prime.

    truncatable(5) 
        ➞ "both"
    # The trivial case of single-digit primes is treated as truncatable from both directions.

    truncatable(139) 
        ➞ False
    # 1 and 9 are non-prime, so 139 cannot be truncatable from either direction.

    truncatable(103) 
        ➞ False
    # Because it contains a 0 digit (even though 103 and 3 are primes).

Notes:
    The input integers will not exceed 10^6.

'''

def truncatable(n):
    def prime(n):
        if n == 2:
            return True
        elif n & 1 == 0 or n == 1:
            return False
        d = 3
        while d * d <= n:
            if n % d == 0:
                return False
            d = d + 2
        return True
        
    if '0' in str(n):
        return False
    l_lft = [str(n)[i:] for i in range(len(str(n)))]
    l_rght = [str(n)[:i+1] for i in range(len(str(n)))]
    pr_l = all([prime(int(i)) for i in l_lft])
    pr_r = all([prime(int(i)) for i in l_rght])
    # return pr_l, pr_r
    le_o, ri_o, bth = pr_l and not pr_r, pr_r and not pr_l, pr_r and pr_l
    return 'left' if le_o else 'right' if ri_o else 'both' if bth else False

if __name__ == "__main__":
    tests = [
        truncatable(47), #"left"
        truncatable(347), #"left"
        truncatable(62383), #"left"
        truncatable(79), # "right"
        truncatable(7331), # "right"
        truncatable(5), # "both", "single-digit number treated as both"
        truncatable(233993), # "right"
    ]
    for t in tests:
        print(t)