''' Challenge: Freshly Made Pizzas [FreshMadePizzas]

Published by Joshua Senoron

Summary:
    Create a Pizza class with the attributes order_number and ingredients (which is given as a list). 
    
    Only the ingredients will be given as input.

    You should also make it so that its possible to choose a ready made pizza flavour 
    rather than typing out the ingredients manually! 
    
    As well as creating this Pizza class, 
    hard-code the following pizza flavours.

        Name	        Ingredients
        hawaiian	    ham, pineapple
        meat_festival	beef, meatball, bacon
        garden_feast	spinach, olives, mushroom

Examples:
    p1 = Pizza(["bacon", "parmesan", "ham"])    # order 1
    p2 = Pizza.garden_feast()                  # order 2
    
    p1.ingredients 
        ➞ ["bacon", "parmesan", "ham"]

    p2.ingredients 
        ➞ ["spinach", "olives", "mushroom"]

    p1.order_number 
        ➞ 1

    p2.order_number 
        ➞ 2

Notes:
    All words are given in lowercase.
    See the Resources tab for some helpful tutorials on classes!

Example by Luis Pereira:
class Pizza:
	
	orders = 0
	
	def __init__ (self, ingredients):
		Pizza.orders +=1
		self.order_number = Pizza.orders
		self.ingredients = ingredients

	def hawaiian():
		return Pizza(["ham", "pineapple"])

	def meat_festival():
		return Pizza(["beef", "meatball", "bacon"])	
		
	def garden_feast():
		return Pizza(["spinach", "olives", "mushroom"])

'''

class Pizza:
    order = 0
    def __init__(self, ingredients):
        self.ingredients = ingredients
        self.order_number = self.number()

    @classmethod
    def garden_feast(cls):
        return cls(["spinach", "olives", "mushroom"])

    @classmethod
    def hawaiian(cls):
        return cls(["ham", "pineapple"])

    @classmethod
    def meat_festival(cls):
        return cls(["beef", "meatball", "bacon"])

    @staticmethod
    def number():
        Pizza.order += 1
        return Pizza.order

if __name__ == "__main__":
    p1 = Pizza(["bacon", "parmesan", "ham"])
    p2 = Pizza.garden_feast()
    p3 = Pizza.hawaiian()
    p4 = Pizza.meat_festival()
    p5 = Pizza(["pepperoni", "bacon"])
    my_pizza = Pizza(["cheese", "caviar", "oyster", "uranium"])
    
    tests = [
        p1.ingredients, # ["bacon", "parmesan", "ham"]
        p2.ingredients, # ["spinach", "olives", "mushroom"]
        p3.ingredients, # ["ham", "pineapple"]
        p4.ingredients, # ["beef", "meatball", "bacon"]
        p5.ingredients, # ["pepperoni", "bacon"]
        my_pizza.ingredients, # ["cheese", "caviar", "oyster", "uranium"
        p1.order_number, # 1
        p2.order_number, # 2
    ]
    for t in tests:
        print(t)