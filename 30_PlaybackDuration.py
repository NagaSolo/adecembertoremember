''' Challenge: Playback Duration [PlaybackDuration]

Published by: Mubashir Hassan

Summary:
    Youtube offers different playback speed options for users. 
    
    This allows users to increase or decrease the speed of the video content. 
    
    Given the actual duration and playback speed of the video. 
    
    Calculate the playback duration of the video.

Examples:
    playback_duration("00:30:00", 2) 
        ➞ "00:15:00"

    playback_duration("01:20:00", 1.5) 
        ➞ "00:53:20"

    playback_duration("51:20:09", 0.5) 
        ➞ "102:40:18"

Notes:
    Both durations will be in hh:mm:ss format.

    Playback speed will be upto one decimal place only.

    Time units are expected to be truncated/floored.

'''

def playback_duration(duration, speed):
    hr, mn, sd = int(duration[:2]), int(duration[3:5]), int(duration[6:])
    hr_o = hr//speed
    mn_o = (hr%speed)*60//speed + mn//speed
    sd_o = (mn%speed)*60//speed + sd//speed
    res = ['0'+str(int(i)) if i < 10 else str(int(i)) for i in [hr_o, mn_o, sd_o]]
    return ':'.join(res)

if __name__ == "__main__":
    tests = [
        playback_duration("00:30:00", 2), # "00:15:00"
        playback_duration("01:20:00", 1.5), # "00:53:20"
        playback_duration("09:20:30", 1), # "09:20:30"
        playback_duration("51:20:09", 0.5), # "102:40:18"
        playback_duration("01:20:00", 2.5), # "00:32:00"
        playback_duration("06:50:13", 3), # "02:16:44"
        playback_duration("05:25:19", 1.5), # "03:36:52"
    ]
    for t in tests:
        print(t)