'''Shape in Shape [ShapeInShape]

Published by: Thomas O'Dell

Summary:
    For math class, 
    Matt needs to find out if a shape can fit inside another shape, 
    based solely on their area. 
    
    The only problem is, 
    HE SUCKS AT MATH! 
    
    He has asked you, 
    his older brother, 
    to make a program that will answer all his math questions.

    Write a function that takes two shapes, 
    with different inputs, 
    and returns True if the second shape has an area smaller than the first.

    The inputs will be in a standardized format per shape:

        "Circle, radius"
        "Triangle, Base, Height"
        "Rectangle, Width, Length (if different than Width) "
        "Pentagon, Side"

Examples:
    shape_in_shape("Circle, 3", "Rectangle, 3, 14") 
        ➞ False

    shape_in_shape("Triangle, 10, 5", "Circle, 2") 
        ➞ True

    shape_in_shape("Pentagon, 10", "Circle, 10") 
        ➞ False

Notes:
    Remember, 
    the first item in each string will be the name of the shape, 
    and all relevant data will be provided following said name.

Example by Jacques1313(cleaner):
from math import pi

def shape_in_shape(shape1, shape2):
	shape1=shape1.split(', ')
	shape2=shape2.split(', ')
	area={
		'Circle': lambda x: pi*int(x[0])**2,
		'Rectangle': lambda x: int(x[0])*int(x[1]),
		'Triangle': lambda x: int(x[0])*int(x[1])/2,
		'Pentagon': lambda x: (25+10*5**.5)**.5*int(x[0])**2/4
		}
	return area[shape2.pop(0)](shape2)<area[shape1.pop(0)](shape1)
'''

def shape_in_shape(shape1, shape2):
    from math import pi, sqrt
    shape1, shape2 = shape1.split(','), shape2.split(',')
    if len(shape1) < 3 or len(shape2) < 3:
        shape1.append(1) or shape2.append(1)
    hc = {'Rectangle': 'x*y', 'Circle': 'pi*x**2', 'Triangle': 'x*y//2', 'Pentagon': 'x**2*(sqrt(5*(5+2*sqrt(5))))//5'}
    return eval(hc.get(shape1[0]), {'x':int(shape1[1]), 'y':int(shape1[2]), 'sqrt':sqrt, 'pi':pi}) > eval(hc.get(shape2[0]), {'x':int(shape2[1]), 'y':int(shape2[2]), 'sqrt':sqrt, 'pi':pi})

if __name__ == "__main__":
    tests = [
        shape_in_shape("Circle, 3", "Rectangle, 3, 14"), #False
        shape_in_shape("Circle, 5", "Rectangle, 3, 14"), #True
        shape_in_shape("Circle, 10", "Pentagon, 10"), # True
        shape_in_shape("Pentagon, 10", "Circle, 10"), # False
    ]
    for t in tests:
        print(t)