''' Challenge: The Actual Memory Size of your USB Flash Drive [ActualUSBFlashSize]

Published by: Saber Khakbiz

Summary:
    Create a function that takes the memory size (MS is a string type) as an argument and returns the actual memory size.

Examples
    actual_memory_size(32GB) ➞ 29.76GB

    actual_memory_size(2GB) ➞ 1.86GB

    actual_memory_size(512MB) ➞ 476MB

Notes:
    The actual storage loss on a USB device is 7% of the overall memory size!
    
    If the actual memory size was greater than 1 GB, 
    round your result to two decimal places.

'''

def actual_memory_size(MS):
    ms = ''.join(MS)
    mss = [i for i in ms]
    sz = ''.join(i for i in ms if i.isdigit())
    g_m, g_o = 'G' in mss and 'M' in mss, 'G' in mss and 'M' not in mss
    g_m_f = '{:.0f}'.format((93/100)*float(sz)*1000)+'MB'
    g_o_f = '{:.2f}'.format((93/100)*float(sz))+'GB'
    el = '{:.0f}'.format((93/100)*float(sz))+'MB'
    return g_m_f if g_m else g_o_f if g_o else el

if __name__ == "__main__":
    tests = [
        actual_memory_size("256MB"), # "238MB"
        actual_memory_size("512MB"), # "476MB"
        actual_memory_size("2GB"), # "1.86GB"
        actual_memory_size("8GB"), # "7.44GB"
        actual_memory_size("16GB"), # "14.88GB"
        actual_memory_size("32GB"), # "29.76GB"
        actual_memory_size("1GMB"), # "930MB"
    ]
    for t in tests:
        print(t)