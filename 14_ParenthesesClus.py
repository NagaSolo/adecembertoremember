''' Challenge: Parentheses Clusters [ParenthesesClus]

Published by: Helen Yu

Summary:
    Write a function that groups a string into parentheses cluster. 
    
    Each cluster should be balanced.

Examples:
    split("()()()") ➞ ["()", "()", "()"]

    split("((()))") ➞ ["((()))"]

    split("((()))(())()()(()())") ➞ ["((()))", "(())", "()", "()", "(()())"]

    split("((())())(()(()()))") ➞ ["((())())", "(()(()()))"]

Notes:
    All input strings will only contain parentheses.

    Balanced: 
        Every opening parens ( must eSxist with its matching closing parens ) in the same cluster.

'''

def split(txt):
    tmp, res, c_i, c_j = [], [], 0, 0
    txt_l = [i for i in txt]
    for i in txt_l:
        if i == '(':
            c_i += 1
            tmp.append(i)
        else:
            c_j += 1
            tmp.append(i)
        if c_i == c_j:
            res.append(tmp)
            tmp = []
    return [''.join(c) for c in res]

if __name__ == "__main__":
    tests = [
        split("()()()"), # ["()", "()", "()"]
        split(""), # []
        split("()()(())"), # ["()", "()", "(())"]
        split("(())(())"), # ["(())", "(())"]
        split("((()))"), # ["((()))"]
        split("()(((((((((())))))))))"), # ["()", "(((((((((())))))))))"]
        split("((())()(()))(()(())())(()())"), # ["((())()(()))", "(()(())())", "(()())"]
    ]
    for t in tests:
        print(t)