''' Challenge: Edabit's Encryption Scheme [EdabitEncryption]

Published by: Matt

Summary:
    An English text needs to be encrypted using Edabit’s encryption scheme. 
    
    First, 
    the spaces are removed from the text. 
    
    Let L be the length of this text. 
    
    Then, 
    characters are written into a grid, whose rows and columns have the following constraints:

        For example, 
        the sentence; 
            "if man was meant to stay on the ground god would have given us roots", 
            after removing spaces, 
            is 54 characters long. 
            
            The square root of 54 is between 7 and 8, 
            so it is written in the form of a grid with 7 rows and 8 columns.

            ifmanwas
            meanttos
            tayonthe
            groundgo
            dwouldha
            vegivenu
            sroots
    
    Ensure that rows x column >= L
    
    If multiple grids satisfy the above conditions, 
    choose the one with the minimum area.
    
    rows x columns >= L
    
    The encoded message is obtained by displaying the characters in a column, 
    inserting a space, 
    and then displaying the next column and inserting a space, and so on. 
    
    For example, 
    the encoded message for the above rectangle is:

        imtgdvs fearwer mayoogo anouuio ntnnlvt wttddes aohghn sseoau

Examples:
    encryption(“haveaniceday”) 
        ➞ “hae and via ecy”

        # have
        # anic
        # eday

    encryption(“feedthedog”) 
        ➞ “fto ehg ee dd”

    encryption(“chillout”) 
        ➞ “clu hlt io”

    encryption(“A Fool and His Money Are Soon Parted.”) 
        ➞ "Anoea FdnSr oHeot oiyoe lsAnd aMrP."

Notes:
    Ensure capitalization remains the same in encrypted text.
    
    Do not remove special characters.

'''

def encryption(txt):
    from math import sqrt
    txt = ''.join(txt.split())
    lgt = len(txt)
    cols = round(sqrt(lgt))
    rows = cols+1 if cols**2 < lgt else cols
    return ' '.join([txt[i:lgt:rows] for i in range(rows)])

if __name__ == "__main__":
    tests = [
        encryption("haveaniceday"), # "hae and via ecy"
        encryption("feedthedog"), # "fto ehg ee dd"
        encryption("chillout"), # "clu hlt io"
        encryption("A Fool and His Money Are Soon Parted."), # "Anoea FdnSr oHeot oiyoe lsAnd aMrP."
        encryption("One should not worry over things that have already happened and that cannot be changed."), 
        # "Onvtlphb. noehreae etraentc swttaech hohhddaa oriayann urnvhnng lygeadoe dosapttd"
        encryption(
            "Back to Square One is a popular saying that means a person has to start over, \
            similar to: back to the drawing board."), 
        # "Brpgatroea aeutpo,:dr cOlhessbrd knaartiaa. tertsamcw oismoriki Ssaentltn qayahoaog upinavrtb aonssetho"
    ]
    for t in tests:
        print(t)