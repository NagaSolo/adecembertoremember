''' Challenge: Caesar's Cipher [CaesarCipher]

Published by: Matt

Summary:
    Julius Caesar protected his confidential information by encrypting it using a cipher. 
    
    Caesar's cipher (check Resources tab for more info) shifts each letter by a number of letters. 
    
    If the shift takes you past the end of the alphabet, 
    just rotate back to the front of the alphabet. 
    
    In the case of a rotation by 3, w, x, y and z would map to z, a, b and c.

    Create a function that takes a string s (text to be encrypted) and an integer k (the rotation factor). 
    
    It should return an encrypted string.

Examples:
    caesar_cipher("middle-Outz", 2) 
        ➞ "okffng-Qwvb"

        # m -> o
        # i -> k
        # d -> f
        # d -> f
        # l -> n
        # e -> g
        # -    -
        # O -> Q
        # u -> w
        # t -> v
        # z -> b

    caesar_cipher("Always-Look-on-the-Bright-Side-of-Life", 5)
        ➞ "Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj"

    caesar_cipher("A friend in need is a friend indeed", 20)
        ➞ "U zlcyhx ch hyyx cm u zlcyhx chxyyx"

Notes:
    All test input will be a valid ASCII string.

'''

def caesar_cipher(s, k):
    ''' 56 for Uppercase, 97 for lowercase, implemented modulus to shift'''
    return ''.join(chr((ord(c) - 97 + k) % 26 + 97) if c.isalpha() and c.islower() else chr((ord(c) - 65 + k) % 26 + 65) if c.isalpha() and c.isupper() else c for c in s)

if __name__ == "__main__":
    tests = [
        caesar_cipher("middle-Outz", 2), # "okffng-Qwvb"
        caesar_cipher("Always-Look-on-the-Bright-Side-of-Life", 5), # "Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj"
        caesar_cipher("A friend in need is a friend indeed", 20), # "U zlcyhx ch hyyx cm u zlcyhx chxyyx"
    ]
    for t in tests:
        print(t)