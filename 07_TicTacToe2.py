''' Challenge: Tic Tac Toe [TicTacToe2]

Published by: AniXDownLoe

Summary:
  Create a function that takes a list of character inputs from a Tic Tac Toe game. 
  
  Inputs will be taken from player1 as "X", 
  player2 as "O", 
  and empty spaces as "#". 
  
  The program will return the winner or tie results.

Examples:
  tic_tac_toe([
    ["X", "O", "O"],
    ["O", "X", "O"],
    ["O", "#", "X"]
  ]) ➞ "Player 1 wins"

  tic_tac_toe([
    ["X", "O", "O"],
    ["O", "X", "O"],
    ["X", "#", "O"]
  ]) ➞ "Player 2 wins"


  tic_tac_toe([
    ["X", "X", "O"],
    ["O", "X", "O"],
    ["X", "O", "#"]
  ]) ➞ "It's a Tie"

'''

def tic_tac_toe(inputs):
    lgt = len(inputs)
    rows = [i[0][0] for i in inputs if len(set(i)) is 1]
    cols = [i[0][0] for i in zip(*inputs) if len(set(i)) is 1]
    dia = [''.join(set(inputs[i][i] for i in range(lgt)))]
    c_dia = [''.join(set(inputs[i][~i] for i in range(lgt)))]
    comb = rows + cols + dia + c_dia
    res = [i for i in comb if len(i) is 1]
    return 'It\'s a Tie' if len(res) is 0 else 'Player 1 wins' if res[0] is 'X' else 'Player 2 wins'


if __name__ == "__main__":
    tests = [
        tic_tac_toe([["X", "O", "O"],["O", "X", "O"],["O", "#", "X"]]), # "Player 1 wins"
        tic_tac_toe([["X", "O", "O"],["O", "X", "O"],["O", "#", "O"]]), # "Player 2 wins"
        tic_tac_toe([["X", "O", "O"],["O", "X", "O"],["O", "O", "#"]]), # "It's a Tie"
        tic_tac_toe([["X", "O", "O"],["X", "X", "O"],["X", "O", "#"]]), # "Player 1 wins"
        tic_tac_toe([["X", "#", "O"],["X", "X", "O"],["#", "O", "#"]]), # "It's a Tie"
    ]
    for t in tests:
        print(t)