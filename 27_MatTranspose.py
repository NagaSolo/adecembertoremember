''' Challenge: Matrix Transpose [MatTranspose]

Published by: werdna

Summary:
    In linear algebra, 
    the transpose of a matrix is an operator which flips a matrix over its diagonal; 
        that is, 
        it switches the row and column indices of the matrix A by producing another matrix, 
        often denoted by A^T.

    Create a function that takes a 2D matrix m and returns a 2D matrix (matrix A^T).

Examples:
    makeTranspose([
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]
        ]) ➞ [
        [1, 4, 7],
        [2, 5, 8],
        [3, 6, 9]
    ]

    makeTranspose([
            [1, 2],
            [3, 4],
            [5, 6]
        ]) ➞ [
        [1, 3, 5],
        [2, 4, 6]
    ]

    makeTranspose([
            ["a", "b"]
        ]) ➞ [
        ["a"],
        ["b"]
    ]

'''

def make_transpose(m):
    return [[n for n in l] for l in zip(*m)]

if __name__ == "__main__":
    tests = [
        make_transpose([[1,2,3],[4,5,6],[7,8,9]]), # [[1,4,7],[2,5,8],[3,6,9]]
        make_transpose([[1,2],[3,4],[5,6]]), # [[1,3,5],[2,4,6]]
        make_transpose([[1,2],[3,4],[5,6],[7,8],[9,10]]), # [[1,3,5,7,9],[2,4,6,8,10]]
        make_transpose([[42]]), # [[42]]
        make_transpose([[0.5,0.6,0.8]]), # [[0.5],[0.6],[0.8]]
        make_transpose([[0,0]]), # [[0],[0]]
        make_transpose([["a","b"]]), # [["a"],["b"]]
    ]
    for t in tests:
        print(t)