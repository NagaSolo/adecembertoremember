''' Challenge: Premier League Champions [PLChamp]

Published by: Deep Xavier

Summary:
    Create a function that takes a list of football clubs with properties: 
        name, 
        wins, 
        loss, 
        draws, 
        scored, 
        conceded, 
    
    and returns the team name with the highest number of points. 
    
    If two teams have the same number of points, 
    return the team with the largest goal difference.

    How to Calculate Points and Goal Difference:
        team = { "name": "Manchester United", "wins": 30, "loss": 3, "draws": 5, "scored": 88, "conceded": 20 }

        Total Points = 3 * wins + 0 * loss + 1 * draws = 3 * 30 + 0 * 3 + 5 * 1 = 95 points
    
        Goal Difference = scored - conceded = 88 - 20 = 68

Examples:
    champions([
        {
            "name": "Manchester United",
            "wins": 30,
            "loss": 3,
            "draws": 5,
            "scored": 88,
            "conceded": 20,
        },
        {
            "name": "Arsenal",
            "wins": 24,
            "loss": 6,
            "draws": 8,
            "scored": 98,
            "conceded": 29,
        },
        {
            "name": "Chelsea",
            "wins": 22,
            "loss": 8,
            "draws": 8,
            "scored": 98,
            "conceded": 29,
        }
    ]) 
        ➞ "Manchester United"

Notes:
    Input is a list of teams.

Example by EvgenySH (shorter, cleaner):
    def champions(teams):
        return max(teams, key=lambda x: (x['wins'] * 3 + x['draws'], x['scored'] - x['conceded']))['name']

'''

def champions(teams):
    tp = {d['name']:[d['wins']*3 + d['draws']*1, d['scored'] - d['conceded']] for d in teams}
    return sorted(tp, key=tp.get, reverse=True)

if __name__ == "__main__":
    tests = [
        champions([
            {
                "name": "Manchester United",
                "wins": 30,
                "loss": 3,
                "draws": 5,
                "scored": 88,
                "conceded": 20,
            },
            {
                "name": "Arsenal",
                "wins": 24,
                "loss": 6,
                "draws": 8,
                "scored": 98,
                "conceded": 29,
            },
            {
                "name": "Chelsea",
                "wins": 22,
                "loss": 8,
                "draws": 8,
                "scored": 98,
                "conceded": 29,
                },
            ]), # "Manchester United"
        champions([
            {
                "name": "Manchester City",
                "wins": 30,
                "loss": 6,
                "draws": 2,
                "scored": 102,
                "conceded": 20,
            },
            {
                "name": "Liverpool",
                "wins": 24,
                "loss": 6,
                "draws": 8,
                "scored": 118,
                "conceded": 29,
            },
            {
                "name": "Arsenal",
                "wins": 30,
                "loss": 0,
                "draws": 8,
                "scored": 87,
                "conceded": 39,
            },
        ]), # "Arsenal"

    ]

    for t in tests:
        print(t)