''' Challenge: Free Range [FreeRange]

Published by: Joshua Senoron

Summary:
    Create a function which converts an ordered number list into a list of ranges (represented as strings). 
    Note how some lists have some numbers missing.

Examples:
    numbers_to_ranges([1, 2, 3, 4, 5]) ➞ ["1-5"]

    numbers_to_ranges([3, 4, 5, 10, 11, 12]) ➞ ["3-5", "10-12"]

    numbers_to_ranges([1, 2, 3, 4, 99, 100]) ➞ ["1-4", "99-100"]

    numbers_to_ranges([1, 3, 4, 5, 6, 7, 8]) ➞ ["1", "3-8"]

Notes:
    If there are no numbers consecutive to a number in the list, 
    represent it as only the string version of that number (see example #4).

    Return an empty list if the given list is empty.

'''

def numbers_to_ranges(lst):
    if len(lst) < 1:
        return []
    fst, snd = [lst[0]], []
    for i in range(1, len(lst)):
        if lst[i] - lst[i - 1] != 1:
            fst.append(lst[i])
            snd.append(lst[i-1])
    snd.append(lst[-1])
    return list('{}-{}'.format(r[0],r[1]) if r[0] != r[1] else '{}'.format(r[0]) for r in zip(fst, snd))

if __name__ == "__main__":
    tests = [
        numbers_to_ranges([1, 2, 3, 4, 5]), # ['1-5']
        numbers_to_ranges([6, 7, 8, 10, 11, 12]), # ['6-8', '10-12']
        numbers_to_ranges([1, 2, 3, 6, 7, 8]), # ['1-3', '6-8']
        numbers_to_ranges([1, 3, 4, 5, 6, 7, 8]), # ['1', '3-8']
        numbers_to_ranges([1]), # ['1']
        numbers_to_ranges([3, 4, 5, 10, 11, 12]), # ['3-5', '10-12']
        numbers_to_ranges([1, 2, 3, 4, 99, 100]), # ['1-4', '99-100']
        numbers_to_ranges([]), # []
    ]
    for t in tests:
        print(t)