''' Challenge: Maximum and Minimum Product Triplets [TripletMaxMinProd]

Published by: Helen Yu

Summary:
    Write two functions:

        One that returns the maximum product of three numbers in a list.
        One that returns the minimum product of three numbers in a list.

Examples:
    max_product([-8, -9, 1, 2, 7]) 
        ➞ 504

    max_product([-8, 1, 2, 7, 9]) 
        ➞ 126

    min_product([1, -1, 1, 1]) 
        ➞ -1

    min_product([-5, -3, -1, 0, 4]) 
        ➞ -15

'''

def max_product(lst):
    from itertools import combinations
    from functools import reduce
    comb = list(combinations(lst, 3))
    return max(reduce((lambda x,y: x*y), i) for i in comb)

def min_product(lst):
    from itertools import combinations
    from functools import reduce
    comb = list(combinations(lst, 3))
    return min(reduce((lambda x,y: x*y), i) for i in comb)

if __name__ == "__main__":
    testmx = [
        max_product([1, -1, 1]), # -1
        max_product([1, -1, 1, 1]), # 1
        max_product([-8, -9, 1, 2, 7]), # 504
    ]
    for t in testmx:
        print(t)

    testmn = [
        min_product([1, -1, 1]), # -1
        min_product([1, -1, 1, 1]), # -1
        min_product([-8, -9, 1, 2, 7]), # -126
    ]
    for t in testmn:
        print(t)