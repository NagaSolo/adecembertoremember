''' Challenge: Tic-Tac-Toe [TicTacToe3] 

Published by: Jameel Saeb

Summary:
    Create a function that takes a Tic-tac-toe board 
    and returns "X" if the X's are placed in a way 
    that there are three X's in a row or 
    returns "O" if there is three O's in a row.

Examples:
    who_won([
        ["O", "X", "O"],
        ["X", "X", "O"],
        ["O", "X", "X"]
    ]) ➞ "X"

    who_won([
        ["O", "O", "X"],
        ["X", "O", "X"],
        ["O", "X", "O"]
    ]) ➞ "O"

Notes:
    There are no Ties.
    All places on the board will have either "X" or "O".
    Check Resources for more info.

Example by Persolut:
    def who_won(board):
	    return 'X' if sum(sq == 'X' for row in board for sq in row) == 5 else 'O'

'''

def who_won(board):
    lgt = len(board)
    rows = [set(i) for i in board]
    cols = [set(i) for i in zip(*board)]
    diag = [set(board[i][i] for i in range(lgt))]
    c_di = [set(board[i][~i] for i in range(lgt))]
    sl = rows + cols + diag + c_di
    res = [''.join(i) for i in sl if len(i)==1]
    return res[0]

if __name__ == "__main__":
    tests = [
        who_won([["X", "O", "X"],["X", "O", "O"],["X", "X", "O"],]), # "X"
        who_won([["O", "X", "O"],["X", "X", "O"],["O", "X", "X"],]), # "X"
        who_won([["X", "X", "O"],["O", "X", "O"],["X", "O", "O"],]), # "O"
    ]
    for t in tests:
        print(t)