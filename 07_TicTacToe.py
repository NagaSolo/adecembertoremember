''' Challenge: Tic Tac Toe [TicTacToe]

Pubished by: Matt

Summary:
    Given a 3x3 matrix of a completed tic-tac-toe game, 
    create a function that returns whether the game is a win for "X", "O", or a "Draw", 
    where "X" and "O" represent themselves on the matrix, 
    and "E" represents an empty spot.

Examples:
    tic_tac_toe([
        ["X", "O", "X"],
        ["O", "X",  "O"],
        ["O", "X",  "X"]
    ]) ➞ "X"

    tic_tac_toe([
        ["O", "O", "O"],
        ["O", "X", "X"],
        ["E", "X", "X"]
    ]) ➞ "O"

    tic_tac_toe([
        ["X", "X", "O"],
        ["O", "O", "X"],
        ["X", "X", "O"]
    ]) ➞ "Draw"

Notes:
    Make sure that if O wins, 
    you return the letter "O" and not the integer 0 (zero) and if it's a draw, 
    make sure you return the capitalised word "Draw". 
    
    If you return "X" or "O", 
    make sure they're capitalised too.

Example by zatoichi49 (numpy):
    import numpy as np

    def tic_tac_toe(board):
        lines = np.vstack((board, np.rot90(board), np.diag(board), np.diag(np.rot90(board)))).tolist()
        for i in lines:
            if i in (['X', 'X', 'X'], ['O', 'O', 'O']):
                return i[0]
        return 'Draw'
'''

def tic_tac_toe(board):
    lgt = len(board)
    rows = [set(i) for i in board]
    cols = [set(board[i][j] for i in range(lgt)) for j in range(lgt)]
    dia = [set(board[i][i] for i in range(lgt))]
    c_dia = [set(board[i][~i] for i in range(lgt))]
    comb = rows + cols + dia + c_dia
    res = ''.join(''.join(i for i in r) for r in comb if len(r) == 1)
    return ''.join(i for i in set(res)) if len(set(res)) is 1 else 'Draw'


if __name__ == "__main__":
    tests = [
        tic_tac_toe([["X", "O", "X"],["O", "X", "O"],["O", "X", "X"]]), # "X"
        tic_tac_toe([["O", "O", "O"],["O", "X", "X"],["E", "X", "X"]]), # "O"
        tic_tac_toe([["X", "X", "O"],["O", "O", "X"],["X", "X", "O"]]), # "Draw"
        tic_tac_toe([["X", "O", "O"],["X", "O", "O"],["X", "X", "X"]]), # "X"
        tic_tac_toe([["X", "X", "O"],["O", "O", "X"],["X", "X", "O"]]), # "Draw"
    ]
    for t in tests:
        print(t)