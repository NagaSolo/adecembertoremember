''' Challenge: First Before Second Letter [FirstBeforeSecond]

Published by: Helen Yu

Summary:
    You are given three inputs: 
        a string, one letter, 
        and a second letter.

    Write a function that, 
    returns True if every instance of the first letter occurs before every instance of the second letter.

Examples:
    first_before_second("a rabbit jumps joyfully", "a", "j") 
        ➞ True
    # Every instance of "a" occurs before every instance of "j".

    first_before_second("knaves knew about waterfalls", "k", "w") 
        ➞  True

    first_before_second("happy birthday", "a", "y") 
        ➞ False
    # The "a" in "birthday" occurs after the "y" in "happy".

    first_before_second("precarious kangaroos", "k", "a") 
        ➞ False

Notes:
    All strings will be in lower case.

    All strings will contain the first and second letters at least once.

'''

def first_before_second(s, first, second):
    let_only = [i for i in s if i in [first, second]]
    srt = sorted(let_only,key=[first, second].index)
    return srt == let_only

if __name__ == "__main__":
    tests = [
        first_before_second("a rabbit jumps joyfully", "a", "j"), # True
        first_before_second("knaves knew about waterfalls", "k", "w"), # True
        first_before_second("maria makes money", "m", "o"), # True
        first_before_second("the hostess made pecan pie", "h", "p"), # True
        first_before_second("barry the butterfly flew away", "b", "f"), # True
        first_before_second("moody muggles", "m", "g"), # True
        first_before_second("happy birthday", "a", "y"), # False
        first_before_second("precarious kangaroos", "k", "a"), # False
        first_before_second("maria makes money", "m", "i"), # False
        first_before_second("taken by the beautiful sunrise", "u", "s"), # False
        first_before_second("sharp cheddar biscuit", "t", "s"), # False
        first_before_second("moody muggles", "m", "o"), # False
    ]
    for t in tests:
        print(t)