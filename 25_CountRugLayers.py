''' Challenge: Layers in a Rug [CountRugLayers]

Published by: Helen Yu

Summary:
    Write a function that counts how many concentric layers a rug.

Examples:
    count_layers([
        "AAAA",
        "ABBA",
        "AAAA"
    ]) ➞ 2

    count_layers([
        "AAAAAAAAA",
        "ABBBBBBBA",
        "ABBAAABBA",
        "ABBBBBBBA",
        "AAAAAAAAA"
    ]) ➞ 3

    count_layers([
        "AAAAAAAAAAA",
        "AABBBBBBBAA",
        "AABCCCCCBAA",
        "AABCAAACBAA",
        "AABCADACBAA",
        "AABCAAACBAA",
        "AABCCCCCBAA",
        "AABBBBBBBAA",
        "AAAAAAAAAAA"
    ]) ➞ 5

Notes:
    Multiple layers can share the same component so count them separately (example #2).

    Layers will be horizontally and vertically symmetric.

    There will be at least one layer for each rug.

'''

def count_layers(rug):
    mid_rug = rug[len(rug)//2]
    count = 1
    for i in range(1, (len(mid_rug)//2)+1):
        if mid_rug[i] != mid_rug[i-1]:
            count += 1
    return count

if __name__ == "__main__":
    tests = [
        count_layers(["AAA"]), # 1
        
        count_layers(["AAAA", "AAAA", "AAAA"]), # 1
        count_layers([
            "AAAA", 
            "ABBA", 
            "AAAA"]), # 2
            
        count_layers([
            "AAAAAAAAA", 
            "ABBBBBBBA", 
            "ABBBBBBBA", 
            "ABBBBBBBA", 
            "AAAAAAAAA"]), # 2
            
        count_layers([
            "AAAAAAAAA", 
            "ABBBBBBBA", 
            "ABBAAABBA", 
            "ABBBBBBBA", 
            "AAAAAAAAA"]), # 3
            
        count_layers([
            "AAAAAAAAA", 
            "ABBBBBBBA", 
            "ABCCCCCBA", 
            "ABBBBBBBA", 
            "AAAAAAAAA"]), # 3
            
        count_layers([
            "AAAAAAAAAAA", 
            "AABBBBBBBAA",
            "AABCCCCCBAA",
            "AABCAAACBAA",
            "AABCADACBAA", 
            "AABCAAACBAA",
            "AABCCCCCBAA",
            "AABBBBBBBAA",
            "AAAAAAAAAAA"]), # 5
            
        count_layers([
            "AAAAAAAAAAA", 
            "AABBBBBBBAA",
            "AABCCCCCBAA",
            "AABCAAACBAA",
            "AABCABACBAA", 
            "AABCAAACBAA",
            "AABCCCCCBAA",
            "AABBBBBBBAA",
            "AAAAAAAAAAA"]), # 5
            
        count_layers([
            "AAAAAAAAAAA", 
            "AABBBBBBBAA",
            "AABCCCCCBAA",
            "AABCDDDCBAA",
            "AABCDDDCBAA", 
            "AABCDDDCBAA",
            "AABCCCCCBAA",
            "AABBBBBBBAA",
            "AAAAAAAAAAA"]), # 4
            
        count_layers([
            "FFFFFFFFFFFFFFFFFFFFFFFFF",
            "FFFFFFFFFFFFFFFFFFFFFFFFF",
            "FFFFGGGGGGGGGGGGGGGGGFFFF",
            "FFFFGGGAAAAAAAAAAAGGGFFFF", 
            "FFFFGGGAABBBBBBBAAGGGFFFF",
            "FFFFGGGAABCCCCCBAAGGGFFFF",
            "FFFFGGGAABCDDDCBAAGGGFFFF",
            "FFFFGGGAABCDDDCBAAGGGFFFF", 
            "FFFFGGGAABCDDDCBAAGGGFFFF",
            "FFFFGGGAABCCCCCBAAGGGFFFF",
            "FFFFGGGAABBBBBBBAAGGGFFFF",
            "FFFFGGGAAAAAAAAAAAGGGFFFF", 
            "FFFFGGGGGGGGGGGGGGGGGFFFF", 
            "FFFFFFFFFFFFFFFFFFFFFFFFF", 
            "FFFFFFFFFFFFFFFFFFFFFFFFF"]), # 6
    ]
    for t in tests:
        print(t)