''' Challenge: ASCII Charts (Part 2: Bar Chart) [ASCIIP2BarChart]

Published by: zatoichi49

Summary:
    Given a dictionary containing quarterly sales values for a year, 
    return a string representing a bar chart of the sales by quarter.

    Quarter names (Q1, Q2, Q3, Q4) should appear on the left.
    
    Quarters should be sorted in descending order by value.
    
    Quarters with the same value should be shown in their yearly order (Q1 -> Q4).
    
    Bars should begin with a "|".
    
    Repeat the character "#" to fill the bar, with each character having a value of 50.
    
    A single space comes after the bar, then the sales for that quarter.
    
    If the value is 0, there should be no space after "|".
    
    Use the newline character ("\n") to separate each bar in the chart.

Example #1:
    bar_chart({"Q4": 250, "Q1": 300, "Q2": 150, "Q3": 0})
        ➞ "Q1|###### 300\nQ4|##### 250\nQ2|### 150\nQ3|0"
    
    When printed:

        Q1|###### 300
        Q4|##### 250
        Q2|### 150
        Q3|0

Example #2:
    bar_chart({"Q4": 500, "Q3": 100, "Q2": 100, "Q1": 150})
        ➞ "Q4|########## 500\nQ1|### 150\nQ2|## 100\nQ3|## 100"

    When printed:

        Q4|########## 500
        Q1|### 150
        Q2|## 100
        Q3|## 100

Notes:
    There should be no additional whitespace after each value.

First Craft (passed all tests on own setup, failed on online terminal):
    def bar_chart(results):
        r_d = sorted([(x[1], x[0]) for x in results.items()], key=lambda x: x[0])[::-1]
        return '\n'.join(
            ('{}|'.format(i[1]) + '#'*(i[0]//50) + ' ' +'{}'.format(i[0]))
            if i[0]>0 else ('{}|'.format(i[1]) + '#'*(i[0]//50) +'{}'.format(i[0]))
            for i in r_d)
'''

def bar_chart(results):
    v_s = sorted([i for i in set(results.values())], reverse=True)
    k_l = lambda x: x[0]
    tpl = [sorted([i for i in results.items() if i[1] == j], key=k_l) for j in v_s]
    res = [i for i in tpl for i in i]
    return '\n'.join(('{}|'.format(i[0]) + '#'*(i[1]//50) + ' ' +'{}'.format(i[1])) if i[1]>0 else ('{}|'.format(i[0]) + '#'*(i[1]//50) +'{}'.format(i[1])) for i in res)

if __name__ == "__main__":
    tests = [
        bar_chart({'Q4': 0, 'Q3': 100, 'Q2': 0, 'Q1': 600}), 
        # "Q1|############ 600\nQ3|## 100\nQ2|0\nQ4|0"
        bar_chart({'Q4': 300, 'Q3': 150, 'Q2': 350, 'Q1': 250}), 
        #"Q2|####### 350\nQ4|###### 300\nQ1|##### 250\nQ3|### 150"
        bar_chart({'Q4': 50, 'Q1': 100, 'Q2': 100, 'Q3': 300}), 
        # "Q3|###### 300\nQ1|## 100\nQ2|## 100\nQ4|# 50"
        bar_chart({'Q4': 350, 'Q3': 50, 'Q2': 600, 'Q1': 300}), 
        # "Q2|############ 600\nQ4|####### 350\nQ1|###### 300\nQ3|# 50"
        bar_chart({'Q4': 400, 'Q3': 600, 'Q2': 350, 'Q1': 600}), 
        # "Q1|############ 600\nQ3|############ 600\nQ4|######## 400\nQ2|####### 350"
    ]
    for t in tests:
        print(t)