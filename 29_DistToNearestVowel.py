''' Challenge: Distance to Nearest Vowel [DistToNearestVowel]

Published by: Helen Yu

Summary:
    Write a function that takes in a string and for each character, 
    returns the distance to the nearest vowel in the string. 
    
    If the character is a vowel itself, 
    return 0.

Examples:
    distance_to_nearest_vowel("aaaaa") 
        ➞ [0, 0, 0, 0, 0]

    distance_to_nearest_vowel("babbb") 
        ➞ [1, 0, 1, 2, 3]

    distance_to_nearest_vowel("abcdabcd") 
        ➞ [0, 1, 2, 1, 0, 1, 2, 3]

    distance_to_nearest_vowel("shopper") 
        ➞ [2, 1, 0, 1, 1, 0, 1]

Notes:
    All input strings will contain at least one vowel.

    Strings will be lowercased.

    Vowels are: a, e, i, o, u.

'''

def distance_to_nearest_vowel(txt):
    tpl = [(i,'0') if j in ['a','e','i','o','u'] else (i,j) for i,j in enumerate(txt)]
    v_pos = [i[0] for i in tpl if i[1] == '0']
    return [min(abs(j[0] - i) for i in v_pos) for j in tpl]

if __name__ == "__main__":

    tests = [
        distance_to_nearest_vowel("aaaaa"), # [0, 0, 0, 0, 0]
        distance_to_nearest_vowel("bba"), # [2, 1, 0]
        distance_to_nearest_vowel("abbb"), # [0, 1, 2, 3]
        distance_to_nearest_vowel("abab"), # [0, 1, 0, 1]
        distance_to_nearest_vowel("babbb"), # [1, 0, 1, 2, 3]
        distance_to_nearest_vowel("baaab"), # [1, 0, 0, 0, 1]
        distance_to_nearest_vowel("abcdabcd"), # [0, 1, 2, 1, 0, 1, 2, 3]
        distance_to_nearest_vowel("abbaaaaba"), # [0, 1, 1, 0, 0, 0, 0, 1, 0]
        distance_to_nearest_vowel("treesandflowers"), # [2, 1, 0, 0, 1, 0, 1, 2, 2, 1, 0, 1, 0, 1, 2]
        distance_to_nearest_vowel("pokerface"), # [1, 0, 1, 0, 1, 1, 0, 1, 0]
        distance_to_nearest_vowel("beautiful"), # [1, 0, 0, 0, 1, 0, 1, 0, 1]
        distance_to_nearest_vowel("rythmandblues"), # [5, 4, 3, 2, 1, 0, 1, 2, 2, 1, 0, 0, 1]
        distance_to_nearest_vowel("shopper"), # [2, 1, 0, 1, 1, 0, 1]
        distance_to_nearest_vowel("singingintherain"), # [1, 0, 1, 1, 0, 1, 1, 0, 1, 2, 1, 0, 1, 0, 0, 1]
        distance_to_nearest_vowel("sugarandspice"), # [1, 0, 1, 0, 1, 0, 1, 2, 2, 1, 0, 1, 0]
        distance_to_nearest_vowel("totally"), # [1, 0, 1, 0, 1, 2, 3]
    ]
    for t in tests:
        print(t)