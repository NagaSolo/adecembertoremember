def greatest_impact(lst):
    var = ['Mood', 'Weather', 'Meals', 'Sleep']
    ov = [sum(lst[i][j] for i in range(len(lst))) for j in range(len(lst[0]))]
    ratio = [j/(len(lst)*10) if i != 2 else j/(len(lst)*3) for i,j in enumerate(ov)]
    if len(set(ratio)) == 1:
        return 'Nothing'
    return sorted({k:v for k, v in zip(var,ratio)}, key={k:v for k, v in zip(var,ratio)}.get)[0], ratio

if __name__ == "__main__":
    tests = [
        greatest_impact([[10, 10, 3, 10], [10, 10, 3, 10], [10, 10, 3, 10]]), # 'Nothing', 'No values were ever low.'
        greatest_impact([[3, 2, 3, 9], [10, 9, 3, 8], [5, 6, 2, 9], [1, 1, 2, 10]]), # 'Weather'
        greatest_impact([[8, 9, 3, 10], [2, 10, 1, 9], [1, 9, 1, 8]]), # 'Meals'
        greatest_impact([[10, 9, 3, 9], [1, 8, 3, 4], [10, 9, 2, 8], [2, 9, 3, 2]]), # 'Sleep'
    ]
    for t in tests:
        print(t)