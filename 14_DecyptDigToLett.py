''' Challenge: Decrypt the String from Digits to Letters [DecyptDigToLett]

Published by: Evgeny SH

Summary:
    Given a string s consisting from digits and #, 
    translate s to English lowercase characters as follows:

        Characters ("a" to "i") are represented by ("1" to "9").
        Characters ("j" to "z") are represented by ("10#" to "26#").

Examples:
    decrypt("10#11#12") ➞ "jkab"

    decrypt("1326#") ➞ "acz"

    decrypt("25#") ➞ "y"

Example by zatoichi49 (using regex):
    import re

    def decrypt(s):
            return ''.join(chr(int(i) + 96) for i in re.findall(r'\d{2}(?=#)|\d', s))

'''

def decrypt(s):
    d_k = [str(i) if i<10 else str(i)+'#' for i in range(1,27)]
    d_v = [chr(i) for i in range(97, 123)]
    d = {k:v for k,v in zip(d_k, d_v)}
    for i in d_k[9:]:
        s = s.replace(i, d[i])
    for i in d_k[:9]:
        s = s.replace(i, d[i])
    return s

if __name__ == "__main__":
    tests = [
        decrypt("10#11#12"), # "jkab"
        decrypt("1326#"), # "acz"
        decrypt("25#"), # "y"
        decrypt("12345678910#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#"), 
        # "abcdefghijklmnopqrstuvwxyz"
	]
	
    for test in tests:
        print(decrypt(test))