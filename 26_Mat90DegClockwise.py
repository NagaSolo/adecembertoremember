''' Challenge: Rotate a Matrix 90 Degrees Clockwise [Mat90DegClockwise]

Published by: Luis Pereira

Summary:
  Create a function that receives a square n*n matrix 
  and returns that matrix after it has been rotated 90 degrees in the clockwise direction.

Examples:
  rotate([
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9]
    ]) ➞ [
      [7, 4, 1],
      [8, 5, 2],
      [9, 6, 3]
    ]

  rotate([
      ["a", "b", "c", "d"],
      ["e", "f", "g", "h"],
      ["i", "j", "k", "l"],
      ["m", "n", "o", "p"]
    ]) ➞ [
      ["m", "i", "e", "a"],
      ["n", "j", "f", "b"],
      ["o", "k", "g", "c"],
      ["p", "l", "h", "d"]
    ]

'''

def rotate(mat):
    return [l[::-1] for l in list(zip(*mat))]

if __name__ == "__main__":
    tests = [ 
        rotate([
            [1, 2],
            [3, 4],
        ]), # [[3, 1],[4, 2]]
        
        rotate([
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]
        ]), # [[7, 4, 1],[8, 5, 2],[9, 6, 3]]
        
        rotate([
            ["a", "b", "c"],
            ["d", "e", "f"], 
            ["g", "h", "i"]]), # [["g", "d", "a"],["h", "e", "b"],["i", "f", "c"]]
            
        rotate([
            ["a", "b", "c", "d"],
            ["e", "f", "g", "h"],
            ["i", "j", "k", "l"],
            ["m", "n", "o", "p"]
        ]), # [["m", "i", "e", "a"],["n", "j", "f", "b"],["o", "k", "g", "c"],["p", "l", "h", "d"]]
    ]

    for t in tests:
        print(t)